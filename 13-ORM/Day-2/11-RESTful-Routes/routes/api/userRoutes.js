const router = require('express').Router();
const User = require('../../models/User');

// This route uses async/await with '.catch()' for errors
// and no HTTP status codes
router.get('/', async (req, res) => {
  const userData = await User.findAll().catch((err) => {
    res.json(err);
  });

  res.json(userData);
});

// This route uses async/await with try/catch for errors
// along with HTTP status codes
router.post('/', async (req, res) => {
  try {
    const userData = await User.create(req.body);
    // 200 status code means the request is successful
    res.status(200).json(userData);
  } catch (err) {
    // 400 status code means the server could not understand the request
    res.status(400).json(err);
  }
});

// Update an user
router.put('/:id', async (req, res) => {
  try {
    // Promise based API - Not Recommended
    // User.update(req.body, {
    //   where: { id: req.params.id, username: 'abc' },
    // }).then((updatedUser) => {
    //   User.findAll({ where: { id: req.params.id } }).then((userData) => {
    //     res.json(userData);
    //   });
    // });
    const updatedUser = await User.update(req.body, {
      where: { id: req.params.id },
    });
    const userData = await User.findAll({ where: { id: req.params.id } });
    // const userData = await User.create(req.body);
    // 200 status code means the request is successful
    res.status(200).json(userData);
  } catch (err) {
    // 400 status code means the server could not understand the request
    res.status(400).json(err);
  }
});

module.exports = router;
