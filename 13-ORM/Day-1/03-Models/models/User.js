const { Model, DataTypes } = require('sequelize');
const sequelize = require('../config/connection');

// CREATE TABLE
class User extends Model {}

// id int PK AI,
// name text,
// email text

User.init(
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: DataTypes.STRING1 },
    email: { type: DataTypes.TEXT },
  },
  { sequelize, timestamps: true, modelName: 'users' }
);
