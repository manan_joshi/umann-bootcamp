const Sequelize = require('sequelize');
require('dotenv').config();

const sequelize = new Sequelize(
  // 'library_db'
  process.env.DB_NAME,
  // 'root'
  process.env.DB_USERNAME,
  // 'myMySQLPassword'
  process.env.DB_PASSWORD,
  {
    host: 'localhost',
    dialect: 'mysql',
    port: 3306,
  }
);

module.exports = sequelize;
