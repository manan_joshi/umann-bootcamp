const router = require('express').Router();
const { Driver, License, Car } = require('../../models');

const unused = 'unused';

// GET all drivers
router.get('/', async (req, res) => {
  try {
    const driverData = await Driver.findAll({
      include: [{ model: License }, { model: Car }],
    });

    res.status(200).json(driverData);
  } catch (err) {
    res.status(500).json(err);
  }
});

router.get('/license-data', async (req, res) => {
  try {
    const licenseData = await License.findAll({
      include: [{ model: Driver }],
    });

    res.status(200).json(licenseData);
  } catch (err) {
    res.status(500).json(err);
  }
});

// GET a single driver
router.get('/:id', async (req, res) => {
  try {
    const driverData = await Driver.findByPk(req.params.id, {
      include: [{ model: License }, { model: Car }],
    });

    if (!driverData) {
      res.status(404).json({ message: 'No driver found with that id!' });
      return;
    }

    res.status(200).json(driverData);
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router;
