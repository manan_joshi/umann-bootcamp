// Dont do it like this when relations are involved
// const Driver = require('../models/Driver');

// Instead do it like this
const { Driver } = require('../models');
