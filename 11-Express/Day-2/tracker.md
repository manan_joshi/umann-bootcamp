# Content Tracker

- [x] POST
- [x] Body Parsing
- [x] Data Persistence
- [x] Student activity (POST/Body Parsing/Data Persistence)

---

- [ ] HTML Routes
- [ ] Static Assets
- [ ] Student Activity (HTML Routes/Static Assets)
- [ ] Modular Routing
- [ ] Custom Middleware
- [ ] Student Activity (Modular Routing/Custom Middleware)
