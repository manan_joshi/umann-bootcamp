// CRUD:     Create, Read, Update,    Delete
// Requests: POST,   GET,  PUT/PATCH, Delete
const express = require("express");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Welcome class");
});

app.post("/something/:id", (req, res) => {
  const id = req.params.id;
  const query = req.query;
  const body = req.body;
  const headers = req.headers;
  console.log({ query, headers });
  res.sendStatus(201);
});

app.listen(3001, () => {
  console.log("Server started");
});
