# Week 11 plan

## Day 1

- [ ] Setup
- [ ] Student Activity (Setup)
- [ ] GET
- [ ] Query Params
- [ ] Student activity (GET/Query Params)

---

- [ ] Insomnia
- [ ] POST
- [ ] Body Parsing
- [ ] Student activity (POST/Body Parsing)

## Day 2

- [ ] HTML Routes
- [ ] Static Assets
- [ ] Student Activity (HTML Routes/Static Assets)
- [ ] Data Persistence
- [ ] Student Activity (Data Persistence)

---

- [ ] Custom Middleware
- [ ] Modular Routing
- [ ] Student Activity (Modular Routing/Custom Middleware)

## Day 3

- [ ] Heroku
- [ ] Insomnia Advanced
- [ ] Week Review
- [ ] Mini Project
