const express = require("express");
const router = express.Router();

const htmlRoutes = require("./html-routes");
const apiRoutes = require("./api-routes");

// router.get("/", (req, res) => {
//   res.send("From routes folder");
// });

router.use(htmlRoutes);
router.use("/api", apiRoutes);

router.get("*", (req, res) => {
  res.sendStatus(404).send("Route does not exist");
});

module.exports = router;
