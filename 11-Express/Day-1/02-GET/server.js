const express = require("express");
const path = require("path");
const termData = require("./db/terms.json");
const PORT = 3001;

// HTTP Verbs
/**
 * GET: Getting some data/resource
 * POST: Sending some data
 * PATCH: Update some data/resource
 * DELETE: Delete some data/resource
 */

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello my name is Tom");
});

// http://localhost:3001?name=Matthew&student=true
app.get("/something", (req, res) => {
  console.log(req.query);
  if (req.query.student === "true") {
    res.send(`Hello my name is ${req.query.name} from something route`);
  } else {
    res.send("Cannot access resource");
  }
});

app.post("/something", (req, res) => {
  console.log(req.body);
  return res.json(req.body);
  // if (req.query.student === "true") {
  //   res.send(`Hello my name is ${req.query.name} from something route`);
  // } else {
  //   res.send("Cannot access resource");
  // }
});

app.get("/something/some-other-thing", (req, res) => {
  res.send("Hello my name is Tom from some other thing route");
});

app.get("/api/terms", (req, res) => res.json(termData));

app.get("/something/:firstName/:lastName", (req, res) => {
  console.log(req.params);
  res.send(
    `Hello my name is ${req.params.firstName} ${req.params.lastName} from something route`
  );
});

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});

// Sets up the Express app to handle data parsing

// app.use(express.static("public"));
