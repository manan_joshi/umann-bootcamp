const express = require("express");

const app = express();

const PORT = 3001;

app.get("/", (req, res) => {
  console.log("Home route");
  res.send("hello friend");
});

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});
