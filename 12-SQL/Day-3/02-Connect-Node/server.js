require('dotenv').config();
const express = require('express');
// Import and require mysql2
const mysql = require('mysql2');

const PORT = process.env.PORT || 3001;
const app = express();

// Express middleware
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const db = mysql.createConnection(
  {
    host: process.env.HOST,
    user: process.env.USERNAME,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
  },
  console.log(`Connected to the classlist_db database.`)
);

// Query database

db.query(
  'INSERT INTO STUDENTS(first_name, last_name, enrolled) VALUES("ABcd", "XYZ", true)',
  function (err, results) {
    console.log({ err, results });
  }
);

db.query('SELECT * FROM students', function (err, results) {
  if (err) console.error(err);
  console.log(results);
});

// Default response for any other request (Not Found)
app.use((req, res) => {
  res.status(404).end();
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
