-- SELECT *
-- FROM course_names
-- JOIN department ON course_names.department = department.id;

-- SELECT course_names.id, course_names.name, department.name from course_names JOIN department ON course_names.department = department.id;



-- JOINS can only be done in SELECT statements

-- SELECT * from <TABLE_NAME> JOIN <JOINING_TABLE> ON <TABLE_NAME>.<FOREIGN_KEY> = <JOINING_TABLE>.<PRIMARY_KEY>
