DROP DATABASE IF EXISTS course_db;
CREATE DATABASE course_db;

USE course_db;

-- Most common Datatypes
-- INT = Number
-- Varchar = variable characters
-- TEXT = Long strings
-- Boolean = true/false
-- DATETIME = Date + Time
-- JSON - JSON data

-- CREATE TABLE <TABLE_NAME> ([<COLUMN_NAME> <DATA_TYPE> <META_DATA>])

CREATE TABLE courses (
  id INT NOT NULL,
  course_title VARCHAR(30) NOT NULL,
  course_description TEXT NOT NULL,
  active BOOLEAN NOT NULL,
  metadata json,
  date_updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
);

