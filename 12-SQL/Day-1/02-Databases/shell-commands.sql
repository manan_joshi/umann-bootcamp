CREATE DATABASE inventory_db;

-- Create two new databases --
DROP DATABASE IF EXISTS inventory_db;
CREATE DATABASE inventory_db;

-- Database 1
-- Database 2
-- Database 3
-- Database 4
-- Database 5
-- Database 6

-- THIS command shows all dbs
SHOW DATABASES;

-- General Syntax: USE <DATABASE_NAME>
-- Use inventory_db --
USE inventory_db;

-- See database in use --
SELECT DATABASE();
