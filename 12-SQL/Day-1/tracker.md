# Content Tracker

- [x] MySQL Shell
- [x] Student Activity (MySQL Shell)
- [x] Databases
- [x] Tables
- [x] Student Activity (Databases/Tables)

---

- [x] Data Types
- [x] Student Activity (DataTypes)
- [x] Schema
- [x] Student Activity (Schema)
