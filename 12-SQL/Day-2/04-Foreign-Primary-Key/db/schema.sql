DROP DATABASE IF EXISTS registrar_db;
CREATE DATABASE registrar_db;

USE registrar_db;


CREATE TABLE instructors (
 uid INT NOT NULL,
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  PRIMARY KEY (id)
);

CREATE TABLE courses (
  id INT,
  course_title VARCHAR(30) NOT NULL,
  instructor_id INT,
  course_title VARCHAR(30) NOT NULL,
  order_details TEXT,
  FOREIGN KEY (instructor_id)
  REFERENCES instructors(id)
);

