-- -- Create a instagram like app

-- -- User ---> Pictures

-- -- User table
-- -- id, name, pictureURL

-- CREATE TABLE USER(id int NOT NULL, name varchar(50), pictureURL TEXT, PRIMARY KEY(id));

-- -- Pictures Table
-- -- id, pictureURL

-- CREATE TABLE PICTURES(id int NOT NUll, pictureURL TEXT, user_id int, FOREIGN KEY REFERENCES USER(user_id))


-- -- User table

-- -- 1 Jordi https://something

-- -- PICTURES

-- -- 7857987 https://some-mioage 1

DROP DATABASE IF EXISTS instagram;
CREATE DATABASE instagram;

USE instagram;

CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(30) NOT NULL,
  pictureURL TEXT
);

CREATE TABLE pictures (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  pictureURL VARCHAR(30) NOT NULL,
  user_id INT,
  FOREIGN KEY (user_id)
  REFERENCES users(id)
);

INSERT INTO users(name) values("ABCD"), ("XYZ"), ("MNO");

SELECT * FROM USERS;

INSERT INTO PICTURES(pictureURL, user_id) VALUES ("https://whatever", 1), ("https://abcd", 2), ('https://xyz', 1);