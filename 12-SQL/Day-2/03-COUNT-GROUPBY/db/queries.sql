SELECT *
FROM course_names;

SELECT department as department_id, COUNT(id) AS something FROM course_names
GROUP BY department;

SELECT department, SUM(total_enrolled) AS sum_enrolled
FROM course_names
GROUP BY department;
