-- General syntax: Update

-- UPDATE <TABLE_NAME> set <Column_Name> = <NEW_VALUE> WHERE CONDITION;

update produce set name="chilly" where name="potato" or id=3;

-- General Syntax: Delete

-- DELETE FROM <TABLE_NAME> WHERE CONDITION;

DELETE from produce where name="chilly";