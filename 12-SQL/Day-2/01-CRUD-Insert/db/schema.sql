-- Create a DB
-- Create a table
-- Run this file

-- Create a DB called inventory

DROP DATABASE IF EXISTS inventory;

CREATE DATABASE inventory;

USE inventory;

CREATE TABLE products(id int, name varchar(100) NOT NULL, isAvailable boolean default TRUE);

ALTER TABLE products