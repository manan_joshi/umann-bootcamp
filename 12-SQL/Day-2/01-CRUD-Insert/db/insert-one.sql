-- Insert row into products table --

-- Genneral Syntx
-- INSERT INTO <TABLE_NAME> VALUES()

-- INSERT INTO products VALUES(1, 'Screw');
-- INSERT INTO products(name) VALUES('Butter');

INSERT INTO products(name) VALUES("Spinach"), ("Potatoes"), ("Tomatoes");
