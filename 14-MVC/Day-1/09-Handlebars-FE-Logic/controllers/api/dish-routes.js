const router = require('express').Router();
const Dish = require('../../models/Dish');

// route to create/add a dish using async/await
router.post('/', async (req, res) => {
  try {
    const dishData = await Dish.create({
      dish_name: req.body.dish_name,
      description: req.body.description,
      guest_name: req.body.guest_name,
      has_nuts: req.body.has_nuts,
    });
    console.log({ dishData });
    // if the dish is successfully created, the new response will be returned as json
    // return res.redirect('/');
    const allDishes = (await Dish.findAll()).map((dish) =>
      dish.get({ plain: true })
    );
    // return res.render('all', {
    //   dishes: allDishes,
    //   guest_name: req.body.guest_name,
    // });
    return res.redirect('/');
  } catch (err) {
    console.log({ err });
    res.status(400).json(err);
  }
});

module.exports = router;
