const router = require('express').Router();
const path = require('path');

// This is the 'get' route
router.get('/', async (req, res) => {
  // Here, index.html is rendered
  res.sendFile(path.join(__dirname, '../views/index.html'));
});

router.get('/user', async (req, res) => {
  try {
    return res.render('info');
  } catch (err) {
    res.send(500);
  }
});

module.exports = router;
