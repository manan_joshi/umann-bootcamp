const router = require('express').Router();
// Here is where we provide hardcoded data to render dynamically
const dishes = [
  {
    dish_name: 'French Bread with Brie Cheese',
    description: 'French baguette with warm brie',
    isBest: true,
  },
  {
    dish_name: 'Cheese Plate with Spanish Chorizo',
    description:
      'Manchego, Iberico, Cabrales, fig jam, grapes, pecans, and Spanich Chorizo',
    isBest: true,
  },
  {
    dish_name: 'Fish Tacos',
    description:
      'Battered/fried fish, corn tortillas, fresh slaw with jalepenos and cilantro, pickled red onion',
    isBest: false,
  },
  {
    dish_name: 'Tropical Fruit Salad',
    description: 'Guava, papaya, pineapple, mango, and star fruit',
    isBest: false,
  },
  {
    dish_name: 'Pork Gyoza',
    description:
      'Homemade japanese dumplings stuffed with a pork and green onion filling',
    isBest: true,
  },
  {
    dish_name: 'Yebeg Tibs with Injera Bread',
    description:
      'Marinated lamb dish with rosemary, garlic, onion, tomato, jalapeño and the East African spice berbere',
    isBest: true,
  },
  {
    dish_name: 'Cape Malay Curry',
    description: 'Chicken and vegitable curry dish with basmati rice',
    isBest: true,
  },
];

//get all dishes
router.get('/', async (req, res) => {
  // ["Cape Malay Curry", "Fish Tacos"]
  res.render('all', { dishes });
});

//get one dish
router.get('/dish/:num', async (req, res) => {
  // This method renders the 'dish' template, and uses params to select the correct dish to render in the template, based on the id of the dish.
  const dish = dishes[req.params.num - 1];

  // const dishFileCOntent = //read the dish.html
  // replace dish.html with the dish content above
  // res.send(dishFileContent)
  return res.render('dish', {
    name: dish.dish_name,
    description: dish.description,
  });
});

module.exports = router;
