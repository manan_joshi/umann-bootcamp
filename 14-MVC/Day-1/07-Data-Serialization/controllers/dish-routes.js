const router = require('express').Router();
const Dish = require('../models/Dish');

// Get all the dishes
router.get('/dishes', async (req, res) => {
  try {
    const dishes = await Dish.findAll();
    const sanitizedData = dishes.map((dish) => dish.get({ plain: true }));
    res.render('all', { dishes: sanitizedData });
  } catch (err) {
    res.status(500).json(err);
  }
});

// get one dish without serializing data
router.get('/dish/:id', async (req, res) => {
  try {
    const dishData = await Dish.findByPk(req.params.id);
    const sanitizedData = dishData.get({ plain: true });
    console.log(dishData);
    res.render('dish', sanitizedData);
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router;
