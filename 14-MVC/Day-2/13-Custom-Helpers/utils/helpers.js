const { format, formatDistance, formatRelative, subDays } = require('date-fns');

module.exports = {
  // the helper method 'format_time' will take in a timestamp and return a string with only the time
  format_time: (date) => {
    // We use the 'toLocaleTimeString()' method to format the time as H:MM:SS AM/PM
    // console.log({ date });
    return formatDistance(new Date(date), new Date(), {
      addSuffix: true,
    });

    // return Intl.DateTimeFormat('en-US', {
    //   // month: 'short',
    //   // day: '2-digit',
    //   // year: '2-digit',
    //   hour: 'numeric',
    // }).format(new Date());
    // return new Date()
    // return date.toLocaleTimeString();
  },
  calculate_sum: (list) => {},
  log: (value) => {
    console.log({ value });
  },
};
